---
title: "Quartiers prioritaires de la ville"
author: "Juliette Engelaere-Lefebvre"
date: "`r Sys.Date()`"
output: html_document
params:
  reg: "52"
  annee: 2024
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE, warning = FALSE)
library(COGiter)
library(datalibaba)
library(tidyverse)
library(sf)
library(mapview)
library(tricky)

schema_zh <- "sial_zonages_habitat"

```

Les quartiers prioritaires de la ville (QPV) sont diffusés par l'ANCT vie data.gouv.fr sur la page https://www.data.gouv.fr/fr/datasets/quartiers-prioritaires-de-la-politique-de-la-ville-qpv/ .

Le présent script de collecte, de préparation et de mise au gabarit régional est exécuté avec les paramètres suivant :

- annee : `r params$annee`
- region : `r params$reg`

# Téléchargements et lecture des données QPV data.gouv

## Contours géo des QPV


```{r download QPV}
fic_contours_qpv <- "ANCT/temp_QPV.geojson"
download.file(url = "https://www.data.gouv.fr/fr/datasets/r/942d4ee8-8142-4556-8ea1-335537ce1119", 
              destfile = fic_contours_qpv)

qpv <- st_read(fic_contours_qpv)
```
La précédente table publiée sous `consultation>habitat_politique_de_la_ville>n_quartiers_prioritaires_pv_s_r52` contient les attributs : 
```{r dic_var}
dic_var <- tribble(
  ~var,         ~lib_var,
  "code_qp",    "Identifiant du QPV",
  "nom_qp",	    "Nom du QPV",
  "commune_qp", "Nom de la ou des commune(s) d'appartenance du QPV (Nom de la commune principale seulement pour les DROM)",
  "the_geom",		"Contour géographique du QPV",
)

select(dic_var, var)
```
Le champ `commune_qp` contient le nom de la commune d'appartenance du QPV. 
En cas de QPV à cheval sur plusieurs communes (par exemple Bellevue sur Nantes/Saint-Herblain), les noms de commune sont concaténés par une virgule. 

## Table communale d'appartenance 
Pour connaître les noms de communes d'appartenance des QPV, l'ANCT diffuse un tableur de données complémentaires qui contient une table de passage entre les code QPV et code communes (COG 2023) nommée `liste-qp2024-hexagone-cog2023.csv`

```{r download cog QPV}

com_QPV <- read_csv2(file = "https://www.data.gouv.fr/fr/datasets/r/4c6bb7f3-97b6-4834-8a3a-f5f8b3e6735b", 
                     locale = locale(encoding = "latin1")) %>% 
  tricky::set_standard_names() %>% 
  select(code_quartier, noms_communes)

```

La table ne concerne que la France métropolitaine (et pas juste la France hexagonale comme le nom du fichier le laissait penser, car la Corse est présente) . 

# Assemblages des code communes et des QPV France entière

## Outre-mer

On identifie les QPV qui n'ont pas de correspondance avec le CSV publié sur data.gouv, puis on utilise une intersection géo avec les tables géo communales de COGiter (en système de projection non translatées) pour les DROM. 

Note : si un QPV métropolitain est absent de la table CSV publiée sur data.gouv, il figurera dans les traitements ci dessous également. 

```{r code communes des qpv des drom}
drom_geo <- bind_rows(
  communes_971_geo %>% st_transform(4326),
  communes_972_geo %>% st_transform(4326),
  communes_973_geo %>% st_transform(4326),
  communes_974_geo %>% st_transform(4326),
  communes_976_geo %>% st_transform(4326)
  )

qpv_drom <- anti_join(qpv, com_QPV, by = "code_quartier") %>% 
  st_transform(4326) %>% 
  st_make_valid() %>% 
  sf::st_join(drom_geo, largest = TRUE) 

# resultat de la jointure : il manque des codes communes
qpv_drom_sans_depcom <- filter(qpv_drom, is.na(DEPCOM)) %>% 
  # le code département en 3 chiffres est présent dans le code quartier après "QV"
  mutate(DEP = str_sub(code_quartier, 3, 5))

# On vérifie que tous les QPV sont ultra-marins (les codes département doivent tous commencer par 9 si outre mer)
qpv_metro_hors_csv <- str_sub(qpv_drom_sans_depcom$DEP, 1, 1) %>% # on prend le premier chiffre de chaque code département
  unique() %>% # dédoublonnage
  # On enlève du vecteur dédoublonné la valeur "9" 
  setdiff("9") %>% 
  # si la longueur du vecteur est différente de zéro, c'est qu'il manque un ou plusieurs QPV dans le CSV
  length(.) != 0 # FALSE attendu

if(qpv_metro_hors_csv) {
  cat("Attention : certains QPV métropolitains sont absent du CSV")
} else {
  cat("Tous les QPV absents du CSV appartiennent bien à un département ultra-marin.")
}
  
```

`r nrow(qpv_drom_sans_depcom)` quartiers prioritaires de la ville n'ont pas pu être associés à une commune. 

Les départements concernés ont pour code `r paste(unique(qpv_drom_sans_depcom$DEP), collapse = ", ")`. 

La Polynésie Francaise (code `987`) et Saint Martin (code `978`) ne sont pas des DROM et à ce titre, sont absents de COGiter.  
On laisse leur champ DEPCOM vide. 

On ajoute aux QPV des DROM dont on connaît le code commune, le nom de la commune principale d'appartenance par une jointure avec la table attributaire des communes de COGiter.
```{r ajout des noms de communes qpv drom}
qpv_drom_com <- qpv_drom %>% 
  left_join(
    select(communes, DEPCOM, NOM_DEPCOM), # table des communes de COGiter avec seulement le code commune et le nom commune
    by = "DEPCOM"
  ) %>% 
  select(names(qpv), noms_communes = NOM_DEPCOM)
```


## Assemblage QPV / Codes communes, métropole et DROM

```{r assemblage}
qpv_com <- qpv %>% 
  # on ne garde que les lignes présentes dans le CSV (a priori France métro)
  inner_join(com_QPV, by =  "code_quartier") %>% 
  # on met en long/lat pour pouvoir assembler avec les DROM
  st_transform(4326) %>% 
  bind_rows(qpv_drom_com) %>% 
  # met les champ dans l'ordre et on les renomme conformément aux noms de l'ancienne table	
  select(code_qp = code_quartier, nom_qp	= nom_quartier, commune_qp = noms_communes, the_geom = geometry)

```

La table `qpv_com` est conforme à la structure de la table précédemment publiée dans consultation, juste elle couvre l'échelle nationale alors que la table précédente était au contours de la région. 



# Découpage aux contours des EPCI de la région.
On ne conserve que les QPV des EPCI de la région en vue de la publication sous consultation. 

```{r emporte pièce regional}
emporte_piece <- epci_geo %>% 
  left_join(epci, by = "EPCI") %>% 
  filter(grepl(params$reg, REGIONS_DE_L_EPCI)) %>% 
  summarise(geometry = st_union(geometry)) %>% 
  st_buffer(5000)

# mapview(emporte_piece)

qpv_com_reg <- qpv_com %>% 
  st_transform(2154) %>% 
  st_filter(emporte_piece)

# Aperçu du résultat
mapview(qpv_com_reg) + mapview(st_centroid(qpv_com_reg))

```

```{r menage, include=FALSE}
# suppression du fichier téléchargé
unlink(x = fic_contours_qpv)

# ménage mémoire interne
rm(com_QPV, drom_geo, emporte_piece, qpv, qpv_drom, qpv_drom_com, qpv_drom_sans_depcom, qpv_metro_hors_csv)
gc()

```

# Chargement des données QPV dans le SGBD

## table nationale
```{r chargement sgbd qpv national, message=FALSE, warning=FALSE}
# le nom des tables nationales 
tab_qpv = c("n_quartiers_prioritaires_pv_s_000", paste0("n_quartiers_prioritaires_pv_s_000_", params$annee))
# versement dans le SGBD
map(.x = tab_qpv, .f = ~poster_data(data = qpv_com, table = .x, schema = schema_zh, pk = 1, 
                                    db = "production", overwrite = TRUE, user = "does"))
# commentaire de la table
comment_qpv <- paste0("Table nationale des Quartiers Prioritaires de la Ville (QPV) constituée à partir des données ANCT publiées sur https://www.data.gouv.fr/fr/datasets/quartiers-prioritaires-de-la-politique-de-la-ville-qpv/")
map(.x = tab_qpv, .f = ~commenter_table(comment = comment_qpv, table = .x, schema = schema_zh, db = "production", user = "does", ecoSQL = FALSE))
```


```{r chargement sgbd qpv regional, message=FALSE, warning=FALSE}
# le nom des tables régionales 
tab_qpv_reg = c("n_quartiers_prioritaires_pv_s_r52", paste0("n_quartiers_prioritaires_pv_s_r52_", params$annee))

# versement dans le SGBD
map(.x = tab_qpv_reg, .f = ~poster_data(data = qpv_com_reg, table = .x, schema = schema_zh, pk = 1, 
                                        db = "production", overwrite = TRUE, user = "does"))
# commentaire de la table
comment_qpv_reg <- gsub("nationale", "régionale", comment_qpv)
map(.x = tab_qpv_reg, .f = ~commenter_table(comment = comment_qpv_reg, table = .x, schema = schema_zh, db = "production", user = "does"))
```


```{r chargement dico attribut, message=FALSE, warning=FALSE}
# Dictionnaire d'attribut
map(.x = c(tab_qpv, tab_qpv_reg), .f = ~post_dico_attr(dico = dic_var, table = .x, schema = schema_zh, db = "production", user = "does"))

```