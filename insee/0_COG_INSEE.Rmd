---
title: "Rapport annuel COG et zonages études INSEE au 1er janv `r params$annee`"
author: "Juliette Engelaere-Lefebvre"
date: "`r format(Sys.Date(), '%d/%m/%Y')`" 
output: 
  html_document:
    df_print: paged
    toc: TRUE
    toc_depth: 4
    toc_float: TRUE
    css: ../gouvdown.css
    keep_md: FALSE
params:
  annee: 2024
  sgbd: TRUE
  reg: "52"
---
```{r setup, include=FALSE, message = FALSE}
# options de compilation rmd
knitr::opts_chunk$set(echo = FALSE, message = FALSE, warning = FALSE)
options(knitr.kable.NA = '')

```

```{r librairies}
# chargements des packages
library(tidyverse)
library(lubridate)
library(knitr)
# devtools::install_github("maeltheuliere/COGiter")
library(COGiter)
# devtools::install_github("antuki/COGugaison")
library(COGugaison)
# devtools::install_github("antuki/CARTElette")
library(CARTElette)
library(readxl)
library(RPostgres)
library(rpostgis)
library(DBI)
library(sf)
library(mapview)
library(htmltools)
library(datalibaba)
nom_reg <- filter(regions, REG == params$reg) %>% pull(NOM_REG) %>% as.character()
```


# Evolution `r params$annee`/`r params$annee -1` du COG et des EPCI

On exploite le package COGugaison pour établir la liste des modifications des contours institutionnels intervenues au 1er janvier `r params$annee`. 
L'ensemble des données originelles provient du [site de l'INSEE](https://www.insee.fr/fr/information/2560452).

## Evolutions des communes de la région (inclut les communes des EPCI limitrophes)

```{r evol_com}
# Table des libellés des types d'évolution des communes
libelle_modif = tribble(
  ~typemodif, ~Modification,
  "f",   "fusion",
  "d",   "scission",
  "c",   "changmt nom"
)

# Tables des zonages communaux INSEE des millésimes N et N-1
table_supracom_Nmoins1 <- get(paste0("table_supracom_", params$annee - 1))
 # COGugaison pas toujours à jour quand on lance le projet, si besoin on télécharge la dernière table INSEE
if(exists(paste0("table_supracom_", params$annee))) {
  table_supracom_N <- get(paste0("table_supracom_", params$annee)) 
  lib_supra_com_N <- get(paste0("libelles_supracom_", params$annee))
  tab_supracom_N_pas_ok <- length(names(table_supracom_N)) < 14 # en cas de mise à jour incomplete de COGugaison
} else {
  tab_supracom_N_pas_ok <- TRUE
}

if(tab_supracom_N_pas_ok | !exists(paste0("table_supracom_", params$annee))) {
  # on récupère la table INSEE sur https://www.insee.fr/fr/information/7671844
  dir.create("extdata")
  fich_mil <- paste0("table-appartenance-geo-communes-", substr(params$annee, 1, 4))
  download.file(paste0("https://www.insee.fr/fr/statistiques/fichier/7671844/", fich_mil, ".zip"), 
                destfile = paste0("extdata/", fich_mil, ".zip"), )
  unzip(zipfile = paste0( "extdata/", fich_mil, ".zip"),
        exdir = "extdata")
  table_supracom_N <- read_excel(paste0("extdata/", fich_mil, ".xlsx"), skip = 5)
  lib_supra_com_N <- read_excel(paste0("extdata/", fich_mil, ".xlsx"), sheet = "Zones_supra_communales", skip = 5) 
}

lib_epci_dep_reg_N <- lib_supra_com_N %>% 
  select(-NB_COM) %>% 
  rowid_to_column() %>% 
  pivot_wider(names_from = NIVGEO, values_from = c(CODGEO, LIBGEO)) %>% 
  unnest() %>% 
  select(contains("EPCI"), contains("DEP"), contains("REG")) %>% 
  distinct() %>% 
  rename_with(~gsub("CODGEO_", "", .x)) %>% 
  rename_with(~gsub("LIBGEO_", "NOM_", .x)) 

# liste des epci de la région en N
epci_reg_N <- table_supracom_N %>% 
  filter(REG == params$reg) %>%
  pull(EPCI) %>% 
  unique %>% 
  setdiff("ZZZZZZZZZ")


com_N_moins1 <- table_supracom_Nmoins1 %>% 
  select(cod_Nmoins1 = CODGEO, lib_com_Nmoins1 = LIBGEO) %>% 
  filter(!is.na(cod_Nmoins1))

com_N <- table_supracom_N %>% 
  rename_with(~ gsub("CODGEO", "DEPCOM", .x)) %>% 
  rename_with(~ gsub("LIBGEO", "NOM_DEPCOM", .x))

if(exists(paste0("table_supracom_", params$annee))) {
  evol_com <- get(paste("PASSAGE", params$annee - 1, params$annee, sep = "_")) 
} else {
  evol_com <- readr::read_csv("https://www.insee.fr/fr/statistiques/fichier/7766585/v_mvt_commune_2024.csv", 
                              col_types = cols(MOD = col_character(), DATE_EFF = col_date(), 
                                               TYPECOM_AV = col_factor(levels = c("COM", "COMA", "COMD", "ARM")), 
                                               TYPECOM_AP = col_factor(levels = c("COM", "COMA", "COMD", "ARM")))) %>% 
    separate(DATE_EFF, into = c("ANNEE", "MOIS", "JOUR"), sep = "-", remove = FALSE ) %>% 
    mutate(ANNEE = as.numeric(ANNEE),
           mil_tab = if_else(paste0(JOUR, MOIS)=="0101", paste("PASSAGE", ANNEE-1, ANNEE, sep = "_"), 
                             paste("PASSAGE", ANNEE, ANNEE + 1, sep = "_")),
           annee = paste(JOUR, MOIS, ANNEE, sep = "/")) %>% 
    filter(MOD %in% c("21", "32", "41", "50") | substring(COM_AV, 1, 2) != substring(COM_AP, 1, 2), 
           TYPECOM_AV == "COM", TYPECOM_AP == "COM") %>% 
    filter(grepl(paste0("_", params$annee, "$"), mil_tab)) %>% 
    mutate(ratio = if_else(MOD %in% c("32", "41", "50"), 1, NA_real_),
           typemodif = case_when(MOD == "32" ~ "f",
                                 MOD %in% c("50", "41") ~ "c",
                                 MOD == "21" ~ "d")) %>% 
    select(COM_AV, COM_AP, annee, typemodif, ratio) %>% 
    rename_with(~ gsub("COM_AV", paste0("cod", params$annee - 1), .x)) %>% 
    rename_with(~ gsub("COM_AP", paste0("cod", params$annee), .x)) 

}


## Reprise du code à appliquer à la table préparée dans COGugaison
evol_com <- evol_com %>% 
  rename_with(~ gsub(params$annee, "_N", .x)) %>% 
  rename_with(~ gsub(params$annee - 1, "_Nmoins1", .x)) %>% 
  left_join(com_N, by = c("cod_N" = "DEPCOM")) %>% 
  right_join(com_N_moins1, .) %>% 
  right_join(libelle_modif, .) %>% 
  # ajout des libellés EPCI
  left_join(select(lib_epci_dep_reg_N, contains("EPCI"))) %>% 
  # ajout des libellés départementaux
  left_join(select(lib_epci_dep_reg_N, contains("DEP"))) %>% 
  # ajout des libellés régionaux
  left_join(select(lib_epci_dep_reg_N, contains("REG"))) %>% 
  # ajout d'un boleen pour indentifier la région
  mutate(reg_sel = EPCI %in% epci_reg_N | params$reg == REG) %>% 
  select(Modification, cod_Nmoins1, lib_com_Nmoins1, cod_N, NOM_DEPCOM, EPCI, NOM_EPCI,
         contains("DEP"), contains("REG"), ratio, annee, reg_sel)

rm(libelle_modif)

```
A l'échelle de la France, `r nrow(evol_com)` communes ont évolué au cours de `r params$annee -1`. 

```{r typ_modif_fce, eval = nrow(evol_com)>0}
evol_com %>% group_by(Modification) %>% summarise(nombre = n_distinct(cod_N)) %>% kable(padding = 3, caption = "Evolutions communales France entière", format  = "simple")
```

En `r nom_reg`, on observe :
```{r type_modif_reg}
a <- evol_com %>% filter(reg_sel) %>% group_by(Modification) %>% summarise(nombre = n_distinct(cod_N))
chgmt_com_reg <- nrow(a) > 0
if(chgmt_com_reg) {
  b <- "Emplacement nouvelle(s) commune(s) :"
  kable(a, padding = 3, caption = "Evolutions communales de la région", format  = "simple")
} else {
  b <- ""
  "Pas de changement dans la région" 
  }
```

```{r viz_modif_reg, eval=chgmt_com_reg}

filter(evol_com, reg_sel) %>% 
  select(-annee, -reg_sel, -REG, -NOM_REG, -NOM_DEP, -EPCI) %>% 
  relocate(ratio, .after = last_col()) %>% 
  kable(caption = "Evolutions communales de la région")

```

`r b`

```{r recup fond carto, include = FALSE}

# CARTelette n'est plus mis à jour, on utilise COGiter et ses tags (versions) pour les contours géo postérieurs à 2021
millesimes_COGiter <- tribble(
  ~Annee, ~tag, 
  2022, "v0.0.9", 
  2023, "v0.0.11",
  2024, "v0.0.13"
  )
tag_COGiter_Nmoins_1 <- filter(millesimes_COGiter, Annee == params$annee - 1) %>% pull(tag)
tag_COGiter_N <- filter(millesimes_COGiter, Annee == params$annee) %>% pull(tag)

# une fonction pour telecharger les contours com de COGiter des anciens millésimes
get_com_geo_old <- function (tag, mil) {
  download.file(paste0("https://github.com/MaelTheuliere/COGiter/raw/", tag, "/data/communes_geo.rda"), 
                destfile = paste0("insee/communes_geo_cogiter_", mil,".rda"))
  load(paste0("insee/communes_geo_cogiter_", mil,".rda")) 
  communes_geo %>% select(INSEE_COM = DEPCOM)
}

# contours com N moins 1
if(length(tag_COGiter_Nmoins_1) > 0) {
  com_geo_Nmoins1 <- get_com_geo_old(tag = tag_COGiter_Nmoins_1, mil = params$annee - 1)
} else {
  com_geo_Nmoins1 <- charger_carte(COG = params$annee - 1, nivsupra = "COM")
}

# contours com N 
if(params$annee == year(today())) {
  com_geo_N <- COGiter::communes_geo %>% 
    select(INSEE_COM = DEPCOM)
} else if(length(tag_COGiter_N) > 0) {
  com_geo_N <- get_com_geo_old(tag = tag_COGiter_N, mil = params$annee)
} else  {
  com_geo_N <- charger_carte(COG = params$annee, nivsupra = "COM")
}

save(com_geo_N, com_geo_Nmoins1, file = paste0("../contours_com_", params$annee-1, "-", params$annee,".RData"))
# load(paste0("../contours_com_", params$annee-1,"-", params$annee,".RData"))

```

```{r carto_modif_reg, eval=chgmt_com_reg}
# Couches des nouvelles communes
c <- filter(evol_com, reg_sel) %>% 
  select(cod_N, NOM_DEPCOM) %>% 
  distinct() %>% 
  inner_join(com_geo_N %>% select(INSEE_COM), ., by = c("INSEE_COM" = "cod_N")) %>% 
  mutate(across(where(is.factor), as.character))

# couche des anciennes communes
d <- filter(evol_com, reg_sel) %>% 
  select(cod_Nmoins1, lib_com_Nmoins1) %>% 
  distinct() %>% 
  inner_join(com_geo_Nmoins1, ., by = c("INSEE_COM" = "cod_Nmoins1")) %>% 
  mutate(across(where(is.factor), as.character)) 

reg <- filter(regions_geo, REG == params$reg) %>% 
  st_cast("MULTILINESTRING")

mapview(list("Annee N" = c, "Annee N-1" = d, "Région" = reg), zcol = list("NOM_DEPCOM", NULL, NULL), 
        label = list(c$NOM_DEPCOM, d$lib_com_Nmoins1, NULL), burst = list(TRUE, FALSE, FALSE),
        color = "grey", col.regions = c(mapviewGetOption("vector.palette"), "grey", NA), alpha.regions = list(1, 0.4, 1),
        legend = list(TRUE, FALSE, FALSE), homebutton = list(FALSE, FALSE, TRUE), map.types = "CartoDB.Positron")@map %>%  
  leaflet::addControl(h4(paste0("Communes modifiéees au 01/01/", params$annee)), position = "bottomright", className="map-title")

rm(a, b, c, d, reg, chgmt_com_reg)
```


## Evolution des contours des départements de la région

```{r evol_dep}

evol_dep <- filter(evol_com, substr(cod_Nmoins1, 1, 2) != as.character(DEP)) 
evol_dep_reg <- filter(evol_dep, reg_sel)

chgmt_dep_nat <- nrow(evol_dep) > 0
chgmt_dep_reg <- nrow(evol_dep_reg) > 0

message_dep_nat <- case_when(
  nrow(evol_dep) == 0 ~ "aucune commune n'a changé de département au cours de l'année ",
  nrow(evol_dep) == 1 & nrow(evol_dep_reg) == 1 ~ "une commune a changé de département au cours de l'année ",
  nrow(evol_dep) == 1 ~ "une commune a changé de département au cours de l'année ",
  nrow(evol_dep) > 1 ~ paste(nrow(evol_dep), "communes ont changé de département au cours de l'année ")
)

message_dep_reg <- case_when(
  nrow(evol_dep) == 0 ~ ".",
  nrow(evol_dep_reg) == 0 ~ paste0(", la région ", nom_reg, " n'est pas concernée."),
  nrow(evol_dep) == 1 & nrow(evol_dep_reg) == 1 ~ ". Elle est située dans la région.",
  nrow(evol_dep_reg) == 1 ~  paste0(", dont une de la région ", nom_reg, "."),
  nrow(evol_dep_reg) > 1 ~ paste0(", dont ", nrow(evol_dep_reg), " communes de la région ", nom_reg, ".")
)

commentaires_dep <- ""
```

A l'échelle nationale, `r message_dep_nat``r params$annee - 1``r message_dep_reg`   

```{r, eval=chgmt_dep_reg}
rm(message_dep_reg, message_dep_nat)

commentaires_dep <- evol_dep %>% 
  mutate(dep_Nmoins1 = substring(cod_Nmoins1, 1, 2)) %>% 
  arrange(desc(reg_sel)) %>% 
  mutate(phrase = paste0("- En raison d'une ", Modification, " de communes, ", lib_com_Nmoins1, " (", cod_Nmoins1, 
                         ") quitte le ", dep_Nmoins1, " et rejoint le ", DEP, " au sein de la commune ",
                         NOM_DEPCOM, " (", cod_N, ") et de l'EPCI ", NOM_EPCI, ". ")) %>% 
  mutate(phrase = if_else(reg_sel, paste0("**", phrase, "**"), phrase)) %>% 
  pull(phrase) %>% 
  paste0(collapse = "<br>")

```

`r commentaires_dep`

## Evolution des EPCI de la région

Le contour des EPCI peut évoluer d'une année sur l'autre de différentes manières :  

- disparition d'un EPCI, par fusion avec un autre par exemple,  
- création d'un nouvel EPCI, par le départ de plusieurs communes d'un ou plusieurs autres EPCI,  
- transferts de communes au sein des EPCI. 

Les deux premiers types de modifications ont un impact sur la liste des EPCI de la région, elles sont facilement repérables, tandis que le dernier type de modification est plus difficilement perceptible. 
En effet, on ne peut pas simplement se baser sur l'évolution du nombre de communes de chacun d'eux car le nombre des communes est susceptible d'évoluer également de son côté. 

### Y a-t-il du changement dans la liste des EPCI de la région entre `r params$annee - 1` et `r params$annee` ?

```{r evol nb EPCI}
rm(commentaires_dep, evol_dep, evol_dep_reg)
# les départements de la région
dep_reg <- filter(communes, REG == params$reg) %>% 
  pull(DEP) %>% 
  unique %>% 
  as.character

# liste des codes epci de la région en N-1
epci_reg_Nmoins1 <- table_supracom_Nmoins1 %>% 
  filter(REG == params$reg) %>%
  pull(EPCI) %>% 
  unique %>% 
  setdiff("ZZZZZZZZZ")

# Libellés du millésime N-1
lib_supra_com_Nmoins1 <- get(paste0("libelles_supracom_", params$annee - 1))


# Y-a-t-il des EPCI nouveaux ?
EPCI_new <- setdiff(epci_reg_N, epci_reg_Nmoins1) %>% 
  tibble(EPCI = .) %>% 
  left_join(lib_supra_com_N %>% select(CODGEO, LIBGEO), by = c("EPCI" = "CODGEO"))
EPCI_new_bol <- nrow(EPCI_new) > 0

# Des EPCI ont-ils disparus ?
EPCI_old <- setdiff(epci_reg_Nmoins1, epci_reg_N) %>% 
  tibble(EPCI = .) %>% 
  left_join(lib_supra_com_Nmoins1 %>% select(CODGEO, LIBGEO), by = c("EPCI" = "CODGEO"))
EPCI_old_bol <- nrow(EPCI_old) > 0

EPCI_nb_message <- case_when(
  nrow(EPCI_new) + nrow(EPCI_old) == 0 ~ "Il n'y pas eu d'évolutions dans la liste des EPCI dans la région.",
  nrow(EPCI_old) == 0 ~ "Il y eu au moins une création d'EPCI dans la région et aucun ancien EPCI n'a par ailleurs disparu.",
  nrow(EPCI_new) == 0 ~ "Il y eu au moins une disparition d'EPCI dans la région sans qu'aucun nouvel EPCI n'ait par ailleurs été créé.",
  TRUE ~ "Il y eu au moins une disparition d'EPCI et une création de nouvel EPCI dans la région."
  
)
```

`r EPCI_nb_message`

```{r epci new, eval = EPCI_new_bol}
knitr::kable(EPCI_new, caption = paste0("Nouveaux EPCI en ", nom_reg, " au 01/01/", params$annee)) %>%
  kableExtra::kable_styling(bootstrap_options = c("striped", "hover", "condensed"))
```


```{r epci old, eval = EPCI_old_bol}
knitr::kable(EPCI_old, caption = paste0("EPCI ayant disparus au 01/01/", params$annee, " (", nom_reg,")" )) %>%
  kableExtra::kable_styling(bootstrap_options = c("striped", "hover", "condensed"))
```


### Y a-t-il des transferts de communes entre EPCI de la région entre `r params$annee - 1` et `r params$annee` ?

```{r evol compo EPCI}
# ménage
rm(EPCI_new, EPCI_old, EPCI_new_bol, EPCI_old_bol, EPCI_nb_message)

# les communes des epci de la région en N-1 (les communes de la région + communes des EPCI limitrophes)
table_com_epci_Nmoins1 <- table_supracom_Nmoins1 %>% 
  select(cod_Nmoins1 = CODGEO, libcom_Nmoins1 = LIBGEO, EPCI_Nmoins1 = EPCI) %>%
  filter(EPCI_Nmoins1 %in% epci_reg_Nmoins1) %>%
  # on ajoute le libellé des EPCI N-1 à partir de leur siret
  inner_join(lib_supra_com_Nmoins1, by = c("EPCI_Nmoins1" = "CODGEO")) %>%
  select(cod_Nmoins1, libcom_Nmoins1, EPCI_Nmoins1, LIB_EPCI_Nmoins1 = LIBGEO, nb_com_epci_Nmoins1 = NB_COM) %>%
  # on retire les communes hors EPCI qui ne sont pas de la région qui sont venues s'incruster
  filter(EPCI_Nmoins1 != "ZZZZZZZZZ" | (EPCI_Nmoins1 == "ZZZZZZZZZ" & substr(cod_Nmoins1, 1, 2) %in% dep_reg)) 

# les communes des epci de la région en N
table_com_epci_N <- table_supracom_N %>%
  select(cod_N = CODGEO, libcom_N = LIBGEO, EPCI_N = EPCI) %>%
  filter(EPCI_N %in% epci_reg_N) %>%
  # on ajoute le libellé des EPCI N à partir de leur siret
  inner_join(lib_supra_com_N, by = c("EPCI_N" = "CODGEO")) %>%
  select(everything(), LIB_EPCI_N = LIBGEO, -NIVGEO, nb_com_epci_N = NB_COM) %>%
  # on retire les communes hors EPCI qui ne sont pas de la région qui sont venues s'incruster
  filter(EPCI_N != "ZZZZZZZZZ" | (EPCI_N == "ZZZZZZZZZ" & substr(cod_N, 1, 2) %in% dep_reg)) 

# la correspondance communale N-1 / N
  # on sélectionne toutes les communes N-1 de la région et des EPCI inter-régionaux
com_passage_N_Nmoins1 <- table_com_epci_Nmoins1 %>%
  # on y adjoint la concordance avec les communes de l'année N
  left_join(evol_com %>% select(cod_Nmoins1, cod_N), by = c("cod_Nmoins1")) %>%
  # on remplit le code commune de l'annee N avec ccelui de l'année précédente s'il est invariant
  mutate(cod_N = coalesce(cod_N, cod_Nmoins1)) %>%
  select(cod_Nmoins1, libcom_Nmoins1, ends_with("_Nmoins1"), cod_N) %>%
  # et enfin les EPCI de l'année N
  left_join(table_com_epci_N,  by = c("cod_N")) %>%
  arrange(EPCI_Nmoins1, EPCI_N)

# Création d'une vue de synthèse à l'EPCI
vue_epci_N <- com_passage_N_Nmoins1 %>% 
  group_by(EPCI_Nmoins1, LIB_EPCI_Nmoins1, nb_com_epci_Nmoins1, EPCI_N, LIB_EPCI_N) %>%
  summarise(nb_com_Nmoins1_concernees = n_distinct(cod_Nmoins1), .groups = "drop") %>%
  add_count(EPCI_Nmoins1) %>%
  filter(n > 1) %>%
  arrange(EPCI_Nmoins1) %>%
  select(-n) %>% 
  mutate(chgt_epci = EPCI_Nmoins1 != EPCI_N) %>% 
  rename_with(~gsub("Nmoins1", params$annee-1, .x)) %>% 
  rename_with(~gsub("N", params$annee, .x))

# récupération de l'indice de la première ligne avec changement d'EPCI
i_note_lec <- which.max(vue_epci_N$chgt_epci)

# un boléen qui indique s'il y a eu du changements dans les EPCI
chgmt_epci_reg <- nrow(vue_epci_N)>0

if (!chgmt_epci_reg) {
  message_epci_reg <- paste0("Dans la région, il n'y a pas eu de mouvements de communes entre EPCI de ", params$annee - 1, 
                             " et EPCI de ",  params$annee, ".")
  message_com_epci_reg <- ""
  message_entete_epci_reg <- ""
} else {
  message_entete_epci_reg <- paste0("On enregistre ", sum(vue_epci_N$chgt_epci), " mouvement(s) de transfert entre EPCI dans la région : ")
  message_epci_reg <- paste0("Notes de lecture : l'EPCI ", vue_epci_N[i_note_lec,2], ", qui comprenait en ", params$annee -1, " ",
                             vue_epci_N[i_note_lec,3], " communes, a vu ", vue_epci_N[i_note_lec,6], " de ses communes rejoindre l'EPCI ", 
                             vue_epci_N[i_note_lec,5], " en ", params$annee, ".")
  message_com_epci_reg <- "Les communes concernées sont :  "
}
```

`r message_entete_epci_reg`

```{r tab evol EPCI, eval = chgmt_epci_reg}
knitr::kable(vue_epci_N, caption = paste0("Transferts de communes : correspondances entre EPCI ", params$annee - 1, "/",  params$annee, " en ", nom_reg)) %>%
  kableExtra::kable_styling(bootstrap_options = c("striped", "hover", "condensed"))

```
`r message_epci_reg`   

`r message_com_epci_reg`

```{r tab evol com EPCI, eval = chgmt_epci_reg}
com_passage_N_Nmoins1 %>% filter(EPCI_N != EPCI_Nmoins1) %>% 
  select(-contains("nb_com_epci")) %>% 
  knitr::kable(caption = paste0("Mouvements de communes entre EPCI ", params$annee - 1, "/",  params$annee, " en ", nom_reg)) %>%
  kableExtra::kable_styling(bootstrap_options = c("striped", "hover", "condensed"))
```


```{r menage}
rm(list = setdiff(ls(), c("com_geo_N", "com_N", "dep_reg", "drv", "epci_reg_N", "lib_supra_com_N", "nom_reg", "params")))
zinsee <- params$annee>2019
sgbd_insee <- all(params$sgbd, zinsee)
```


# Intégration des zonages d'études INSEE `r params$annee`  

L'INSEE fournit chaque année une [table communale de passage entre différents zonages d'études](https://www.insee.fr/fr/information/2028028).
SIAL/DPH a indiqué vouloir récupérer dans le pack habitat les zonages :  

- Intercommunalités - Métropoles et leur nature,   
- Zones d'emploi 2020,  
- Unités urbaine 2020,  
- Tranches d'unité urbaine 2017, 
- Aires d'attraction des villes 2020, y compris tranches d'AAV et catégories commune AAV 2020.  

Par ailleurs l'INSEE a récemment revu les bassins de vie, on ajoute le zonage BV2022.

Ces traitements ne sont possibles que pour les années postérieures à 2020. 

## Construction de la table des zonages INSEE  

On sélectionne les bonnes variables et on ajoute les libellés correspondants.  
Les communes des EPCI de la région sont identifiées avec le booléen `reg_sel`.


```{r zon_insee, eval=zinsee}

# Sélection des zonages choisis par DPH et ajout des libellés
zon_com_insee <- COGiter::communes_info_supra %>% 
  left_join(COGiter::epci %>% select(EPCI, NATURE_EPCI)) %>% 
  select(-DEPARTEMENTS_DE_L_EPCI, -REGIONS_DE_L_EPCI) %>% 
  relocate(NATURE_EPCI, .before = "NOM_EPCI") %>% 
  mutate(reg_sel = EPCI %in% epci_reg_N | REG == params$reg) %>% 
  left_join(COGiter::table_passage_communes_zonages %>% 
              select(DEPCOM, contains(c("ZE", "UU", "AAV", "BV")))) %>% 
  rename_with(~gsub("LIB_", "NOM_", .x)) %>% 
  distinct() %>% 
  mutate(across(where(is.factor), as.character))


```

## Chargement SGBD

On sauvegarde la table des zonages INSEE France entière dans deux tables SGBD, l'une correspond au nom court `zonages_insee_000`, l'autre comporte dans son nom l'année en cours. Ainsi, l'archivage des couches annuelles se fera automatiquement.

```{r chargement sgbd, eval = sgbd_insee, include = FALSE}
schemaZh <- "sial_zonages_habitat"
table_actu <- "n_zonages_insee_000"
table_mil <- paste0(table_actu, "_", params$annee)

poster_data(data = zon_com_insee, table = table_actu, schema = schemaZh, pk = 1, post_row_name = FALSE, db = "production", overwrite = TRUE, droits_schema = TRUE, user = "does")
poster_data(data = zon_com_insee, table = table_mil,  schema = schemaZh, pk = 1, post_row_name = FALSE, db = "production", overwrite = TRUE, droits_schema = TRUE, user = "does")

commentaire_table <- paste0("Table des zonages d etudes INSEE de ", params$annee, ", mise a jour le ", format(Sys.Date(), '%d %m %Y'), 
                            ". La carte communale est celle de ", params$annee)
commenter_table(commentaire_table, table = table_actu, schema = schemaZh, db = "production", user = "does")
commenter_table(commentaire_table, table = table_mil, schema = schemaZh, db = "production", user = "does")

prep_dico_var <- lister_zonages() %>% 
  filter(`Code du zonage` %in% names(zon_com_insee)) %>% 
  set_names("VAR", "VAR_LIB")

prep_dico_var_lib <- prep_dico_var %>% 
  mutate(VAR = paste0("nom_", VAR), VAR_LIB = paste0("Libellé ", VAR_LIB))


dico_var <- tribble(
  ~VAR, ~VAR_LIB,
  "reg_sel", paste0("Booleen identifiant les communes des EPCI de la region ", nom_reg),
  "DEPCOM", paste0("Code INSEE de la commune (contours de ", params$annee, ")")) %>% 
  bind_rows(prep_dico_var, prep_dico_var_lib)

post_dico_attr(dico = dico_var, table = table_actu, schema = schemaZh, db = "production", user = "does")
post_dico_attr(dico = dico_var, table = table_mil, schema = schemaZh, db = "production", user = "does")


```


## Visualisation zonages INSEE


### Zones d'emplois 2020
```{r viz ZE, eval=zinsee}
# une fonction de viz selon le zonage
viz_zon <- function(zon = "ze2020") {
  zonage <- zon_com_insee %>% 
    rename_with(tolower) %>% 
    filter(reg_sel) %>%
    select(contains("com"), contains(zon))%>% 
    inner_join(
      select(com_geo_N, depcom = INSEE_COM), .
    )
  
  mapview(zonage, zcol = paste0("nom_", zon), label = paste0("nom_", zon))
}

viz_zon(zon = "ze2020")
```
### Bassins vie 2022

```{r viz BV, eval=zinsee}
viz_zon("bv2022")
```


### Unités urbaines 2020

```{r viz UU, eval=zinsee}
viz_zon("uu2020") + viz_zon("tuu2017")
```
### Aires attraction des villes 2020

```{r viz AAV, eval=zinsee}
viz_zon("aav2020") + viz_zon("cateaav2020") + viz_zon("taav2017")
```

```{r}
# menage
list.dirs("extdata") %>% unlink(., recursive = TRUE, force = TRUE)

```


