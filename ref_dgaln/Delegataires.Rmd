---
title: "Intégration des délégataires des aides à la pierre"
author: "Juliette ENGELAERE-LEFEBVRE"
date: "`r format(Sys.Date(), '%d/%m/%Y')`" 
output: 
  html_document:
    df_print: paged
    toc: yes
    toc_depth: '2'
params:
  annee: "2024"
  sgbd: TRUE
  reg: "52"
---

```{r setup, include=FALSE, message = FALSE}
# options de compilation rmd
knitr::opts_chunk$set(echo = TRUE)
options(knitr.kable.NA = '')

# chargements des packages
library(tidyverse)
library(lubridate)
# devtools::install_github("maeltheuliere/COGiter")
library(COGiter)
library(readxl)
library(readODS)
library(datalibaba)
library(sf)
# devtools::install_github("pachevalier/tricky")
library(tricky)
# devtools::install_github("hadley/emo")
library(emo)

# table_passage_com_historique <- COGiter::table_passage_com_historique %>% 
#   # Ajout saint Barthelemy et St Martin
#   bind_rows(data.frame(DEPCOM = c("97701", "97801", "97701", "97801"), DEPCOM_HIST = c("97123", "97127", "97701", "97801"))) %>% 
#   distinct()

sgbd <- params$sgbd
schema_zh <- "sial_zonages_habitat"

# fichiers à lire
repo_ext_data <- "T:/datalab/SIAL/ZONAGES_HABITAT/extdata/ASI/" 
# repo_ext_data <- "../../extdata/ASI/" 
fich <- list.files(repo_ext_data, full.names = FALSE, recursive = TRUE)

fic <- list()
# fichiers pour délégation de compétences 
fic$deleg1 <- grep("delegat", fich, value = TRUE)
fic$deleg2 <- grep("gestionn", fich, value = TRUE)
fic$deleg3 <- grep("typedeleg", fich, value = TRUE)

adr <- paste0(repo_ext_data, fic)

```



# Délegataires des aides à la pierre : production du zonage à partir de la table `r fic$deleg2`

En attendant l'accès à InfoSIAP par API, on utilise les informations 2023 issues de ASI et celles consultées sur https://siap.logement.gouv.fr/admin-administration/gestion-delegataires pour la seule région Pays de la Loire.

Les tables ASI `r fic$deleg1`, `r fic$deleg2`, `r fic$deleg3`, comprennent des informations relatives aux délégataires.

On part de la la table `r fic$deleg2`, listant les délégataires et comprenant leurs noms, codes, SIREN, types (EPCI, Département).  
Les traitements sont les suivants :   

- on écarte les délégataires dont la date de fin de délégation est dépassée,  
- on joint aux délégataires EPCI la composition communale de chacun, puis on joint aux départements délégataires les communes non encore jointes,  
- enfin on affecte les communes restantes aux services départementaux de l'Etat.  

## Production de la table communale des délégataires 
```{r delegataires prod}
# Lecture des donnees ASI éparpillées dans différents fichiers
deleg_codes_00 <- read_excel(adr[2], col_types = "text") 

# modif manuelles PDl 2024 : nouveau delegataire Sables Olonnes
deleg_new <- filter(deleg_codes_00, gest_code == "85191") %>% 
  mutate(gest_code = "85194", gest_siren = "200071165", gest_nom = "CA Les Sables d'Olonne Agglomération", 
         gest_comcent = "85194", gest_debut_deleg = "2024", gest_fin_deleg = "2029")

# modif manuelles PDl 2024 nouvelles dates
deleg_codes_0 <- deleg_codes_00 %>% 
  bind_rows(deleg_new) %>% 
  mutate(
    gest_nom = if_else(gest_code == "44184", "Saint Nazaire Agglomération", gest_nom),
    gest_debut_deleg = case_when(
      gest_code == "CG053" ~ "2024",       # nouveau contrat CD 53 type 3 2024 - 2029
      gest_code == "CG085" ~ "2024",       # nouveau contrat CD 85 type 3 2024 - 2029
      TRUE ~ gest_debut_deleg),
    gest_fin_deleg = case_when(
      gest_code == "44109" ~ "2025",       # prolongation Nantes Métropole 2024 --> 2025
      gest_code == "CG053" ~ "2029",       # nouveau contrat type 3 CD 53 2024 - 2029 
      gest_code == "CG085" ~ "2029",       # nouveau contrat type 3 CD 85 2024 - 2029
      TRUE ~ gest_fin_deleg)) 

rm(deleg_new, deleg_codes_00)

deleg_main_0 <- read_excel(adr[1], col_types = "text")
deleg_main_1 <- deleg_main_0 %>% 
  select(-contains("adr"), -Dele_mel, -Dele_tel) %>% 
  mutate(Dele_siren = str_extract(Dele_siren, "\\d{9}"))

deleg_main <- deleg_main_1 %>% 
# nouveau delegataire Sables Olonnes type 3
  filter(Dele_codegest == "85191") %>% 
  mutate(Dele_codegest = "85194", Dele_siren = "200071165") %>% 
  bind_rows(deleg_main_1) %>% 
  # nouveau contrat 2024 - 2029 type 3 CD 53 & CD 85
  mutate(type_delegat = if_else(Dele_codegest %in% c("CG053", "CG085"), "3", type_delegat))
  

deleg_type_0 <- read_excel(adr[3], col_types = "text")  


# Assemblage   
delegataires <- deleg_codes_0 %>% 
  filter(gest_deleg == "Oui") %>% 
  select(-gest_deleg) %>% 
  # ajout du type de delegataire
  left_join(deleg_main %>% select(Dele_codegest, Dele_siren, Dele_type), 
            by = c("gest_code" = "Dele_codegest", "gest_siren" = "Dele_siren")) %>% 
  left_join(deleg_type_0, by = c("Dele_type" = "Tdel_code"))

# association des communes aux EPCI délégataires
comm_deleg_epci <- filter(delegataires, tdel_nom == "EPCI") %>% 
  left_join(select(communes, DEPCOM, NOM_DEPCOM, EPCI), by = c("gest_siren" = "EPCI")) %>% 
  select(DEPCOM, NOM_DEPCOM, gest_code, gest_nom)

# association des communes aux CD délégataires
comm_deleg_dep <- filter(delegataires, tdel_nom == "departement") %>% 
  # on apparie d'abord avec toutes les communes du département
  left_join(select(communes, DEPCOM, NOM_DEPCOM, DEP), by = c("gest_depref" = "DEP")) %>% 
  # puis on enlève les lignes communales déjà sous gestionnaire EPCI
  anti_join(comm_deleg_epci, by = "DEPCOM") %>% 
  select(DEPCOM, NOM_DEPCOM, gest_code, gest_nom) %>% 
  mutate(gest_nom = gsub("CD ", "Dpt ", gest_nom))

# association des communes à leur services départementaux
comm_svc_dep <- select(communes, DEPCOM, NOM_DEPCOM, DEP) %>% 
  bind_rows(data.frame(DEPCOM = c("97701", "97801"), NOM_DEPCOM = c("Saint-Barthélemy", "Saint-Martin"), DEP = c("977", "978"))) %>% 
  mutate(gest_code = if_else(nchar(as.character(DEP)) == 2, paste0("DD0", DEP),  paste0("DD", DEP))) %>% 
  left_join(select(deleg_codes_0, gest_code, gest_nom), by = "gest_code") %>% 
  select(-DEP) %>% 
  # on enlève ce qui est déjà présent dans les 2 premières tables
  anti_join(comm_deleg_epci, by = "DEPCOM") %>% 
  anti_join(comm_deleg_dep, by = "DEPCOM")

# Compilation des trois tables communales
comm_deleg <- bind_rows(comm_deleg_epci, comm_deleg_dep, comm_svc_dep) %>% 
  arrange(DEPCOM)
  
rm(comm_deleg_epci, comm_deleg_dep, comm_svc_dep, deleg_codes_0, deleg_main_0, deleg_main, deleg_type_0)

```

## Assemblage géographique des communes et ajout attributs

On assemble les communes pour créer les contours géographiques des délégataires puis on joint les attributs descriptifs de chaque délégataire.    
Visualisation brute de la couche produite (en écartant les gestionnaires services de l'Etat) : 

```{r delegataires geo}
delegataires2 <- comm_deleg %>% 
  left_join(communes %>% select(DEPCOM, REG)) %>% 
  mutate(r52 = (REG == params$reg)) %>% 
  right_join(select(communes_geo, -AREA), ., by = c("DEPCOM")) %>% 
  group_by(gest_code, gest_nom) %>%
  summarise(nb_communes = n(), r52 = any(r52), do_union = TRUE, is_coverage = FALSE, .groups = "drop") %>%
  left_join(delegataires %>% select(-gest_nom), by = c("gest_code")) %>% 
  rename_with(.fn = ~ gsub("gest_", "dele_", .x)) %>% 
  select(id_dele = dele_code, dele_nom, debut_deleg = dele_debut_deleg, fin_deleg = dele_fin_deleg, type_dele = tdel_nom,
         dele_siren, dep = dele_depref, com_centre = dele_comcent, nb_communes, r52, the_geom = geometry)
  
delegataires2 %>% 
  filter(r52, !is.na(type_dele)) %>% 
  st_set_crs(2154) -> a

mapview::mapview(x = a, zcol = "dele_nom", burst = FALSE, legend = FALSE)
```


## Alimentation / mise à jour du SGBD

On sauvegarde la table des délégataires France entière dans deux tables SGBD, l'une correspond au nom court `r_delegataires_000`, l'autre comporte dans son nom l'année en cours. Ainsi, l'archivage des couches annuelles se fera automatiquement.

```{r delegataires sgbd, message=FALSE, warning=FALSE, eval=sgbd}
# nom des tables
tbl_deleg <- c("r_delegataires_000", paste0("r_delegataires_000_", params$annee) )

# versement dans le SGBD
map(.x = tbl_deleg, .f = ~poster_data(data = delegataires2, table = .x, schema = schema_zh, pk = 1, db = "production", overwrite = TRUE, user = "does"))

# commentaire de la table
cmtr <- c("Table des délégataires des aides à la pierre France entiere, mise à jour 2024 pour Pays de la Loire, 2023 pour le reste de la France, en attendant une source d'info nationale (RefSIL).",
          paste0("Table des délégataires des aides à la pierre France entiere ",  params$annee, " pour les Pays de la Loire, 2023 en attendant l'accès à RefSIL sinon."))
map2(.x = tbl_deleg, .y = cmtr, .f = ~commenter_table(comment = .y, table = .x, schema = schema_zh, db = "production", user = "does"))

# commentaires de champs

dico_var <- tribble(
  ~"VAR", ~"VAR_LIB",
  "id_dele", "Identifiant de l'organisme délégataire des aides à la pierre",
  "dele_nom", "Nom de l'organisme délégataire des aides à la pierre",
)
map(.x = tbl_deleg, .f = ~post_dico_attr(dico = dico_var, table = .x, schema = schema_zh, db = "production", user = "does", ecoSQL = FALSE))


```
On dépose également une table communale 'delegataires_com_000' indiquant pour chaque commune son délégataire. 
```{r delegataires com sgbd, message=FALSE, warning=FALSE, eval=sgbd}
# versement dans le SGBD de la table communale intermédiaire
poster_data(data = comm_deleg %>% rename(id_dele = gest_code, dele_nom = gest_nom), user = "does", 
            table = "delegataires_com_000", schema = schema_zh, pk = 1, db = "production", overwrite = TRUE)

cmtr_com <- c("Table communale intermédiaire nécessaire à la production et la maintenance de la couche r_delegataires_000. Elle fournit la composition communale de l'emprise des délégataires des aides à la pierre France entiere, délagataires 2024 pour la région Pays de Loire, 2023 sinon")

commenter_table(comment = cmtr_com, table = "delegataires_com_000", schema = schema_zh, db = "production")
post_dico_attr(dico = dico_var, table = "delegataires_com_000", schema = schema_zh, db = "production", user = "does", ecoSQL = FALSE)
```

```{r delegataires nettoyage, message=FALSE, warning=FALSE}
rm(comm_deleg, delegataires, tbl_deleg, cmtr, cmtr_com, a)
```

